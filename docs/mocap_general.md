
# Motion Capture - General

Faceit includes interfaces for recording and importing animation data from various motion capture applications. All Faceit motion capture operators are streamlined, so it's easy to switch between the different capturing workflows and combine motions from different sources.  

Faceit comes packed with a bunch of interfaces and utilities that allow to [import recorded animations](mocap_importers.md#motion-capture---importers) or even [record new animations live](mocap_live.md#motion-capture---osc-live) in the Blender viewport. Faceit supports the popular and accessible [ARKit](#apple-arkit), [Hallway Tile](#hallway-tile) and [Audio2Face](#nvidia-audio2face) platforms for high quality performance capture. The animation recorded by either of these apps can be retargeted to FACS based shape keys, like the ARKit or Audio2Face expression sets. Faceit does not offer any interfaces for bone based motion capture. A bunch of apps for PCs, Macs and iOS devices are supported with easy to use interfaces.

![live](img/mocap/live-01.gif)  
*Quickly receive and record animation data from various sources in real-time.*  

<!-- ??? note "What is Motion Capture?"
    The animation data can be captured to drive 3D facial expressions in **CGI productions** and **Games** or streamed to an audience in realtime for **VTubing** or **Theatre Performances**. -->

!!!warning ""
    Additional costs might occur. Please read below for more information on the individual **hardware and software requirements**.  

[TOC]
___

## Apple ARKit

The ARKit (Apple ARKit) is Apple’s augmented reality (AR) development platform for iOS mobile devices. The **ARKit Face Tracking** is a very **powerful** and **easy to use** tool that is shipped to all **iOS devices with the True Depth camera** (see [hardware](#hardware)). The ARKit Face Tracking captures 52 micro expressions in an actors face in realtime. Any 3D character that is equipped with a set of shape keys resembling the [**52 captured micro expressions**](#arkit-expressions) can be animated with the ARKit. Next to the creation of the 52 required shape keys, Faceit also provides a collection of tools that ease the performance capture process right inside Blender.  
___

### Hardware

All [ARKit applications](#software) listed below can only be used on iOS devices with the True Depth camera. The True Depth camera is available on the following devices:  

* **iPhone X, XR, XS, XS Max, 11, 11 Pro, 11 Pro Max, iPhone12, iPhone13...**  
* **iPad Pro 3rd, 4th, 5th generation...**  

___

### Software

This section will give a quick overview of the iOS apps that utilize the ARKit Face Tracking features. Note that all these apps use the same capturing framework built into the ARKit and therefore there shouldn't be any difference in the captured data. Some apps offer additional features, like live streaming animation data, recording and exporting data or amplifying individual expressions to make them more or less expressive.  
___

>#### Live Link Face

>No matter if you are using the Unreal Engine or not, the **Live Link Face App** by Epic Games provides a great way to quickly record or stream ARKit data to vraious applications, including Faceit.    
[Get the app here](https://apps.apple.com/us/app/live-link-face/id1495370836){target=_blank}  
[Find the documenation here](https://docs.unrealengine.com/4.27/en-US/AnimatingObjects/SkeletalMeshAnimation/FacialRecordingiPhone/){target=_blank}  
>!!!tip "Import to Faceit"  
    - [Import recorded animation via CSV file format](mocap_importers.md#live-link-face)  
    - [Record live](mocap_live.md).  

![epic](img/mocap/epic/epic.jpg)
___

>#### Face Cap App

>Stream animation data live via OSC or record your performance and export the result to FBX or TXT.  
[Get the app here](https://apps.apple.com/de/app/face-cap-motion-capture/id1373155478){target=_blank}  
[Find the documenation here](https://www.bannaflak.com/face-cap/){target=_blank}  
>!!!tip "Import to Faceit"  
    - [Import text file format](mocap_importers.md#face-cap)  
    - [Record live](mocap_live.md).  
    - Retarget motions from FBX.  
    ???+ warning "In-App Purchases"  
        The Face Cap app itself is free-to-use, but the live streaming functionality is time capped to 5 seconds. You can unlock unlimited streaming via an in-app purchase (34.99$ - price may vary based on your region).  
![facecap](img/mocap/facecap/fc.jpg)  
![txt02](img/mocap/facecap/panel_config.png)
*Quickly aplify specific shapes.*  
___

>#### iFacialMocap  

>iFacialMocap is another app that allows you to conveniently capture ARKit data on iOS devices. You can quickly record animations and also recieve and record directly in Faceit.  
[Checkout the website for more information and download links!](https://www.ifacialmocap.com/){target=_blank}  
>!!!tip "Import to Faceit"  
    - [Record live](mocap_live.md#ifacialmocap).  
    - [Retarget motions from FBX.](fbx_retargeting.md)  
    ???+ warning "In-App Purchases"  
        Some features are only available with an in-app purchase.  

>#### Other ARKit Apps

>There is a range of other iOS apps that can be used for recording ARKit Face Tracking data. The following list is not exhaustive, but gives an overview of the available apps that offer interfaces to Blender (not within Faceit, but with stand-alone add-ons or other methods).  
>??? info "Facemotion3D"  
    With Facemotion3D, you can capture facial expressions with an iOS app and communicate in real time with 3DCG software on your PC. You can also FBX export the recorded animation data. You can also import VRM format files into the iOS app.  
    Get the app [here](https://apps.apple.com/tt/app/facemotion3d/id1507538005){target=_blank}.  
    Find more information [here](https://www.facemotion3d.info/){target=_blank}.  
    Get the Blender Add-on [here](https://www.facemotion3d.info/downloads/){target=_blank}.  
    ???+ warning "In-App Purchases"  
        Some features are only available with an in-app purchase.  
>??? info "BMC"
    The App [BMC - Blender Motion Capture](https://blenderartists.org/t/bmc-blender-motion-capture/1203530/2){target=_blank} can be used to record facial motion and send it to your computer via email. An Add-on is provided to load the motion in your Blender scene.  
___

## Hallway Tile

[Hallway's Tile](https://joinhallway.com/) is a desktop app that allows to capture the 52 ARKit expressions with a regular webcam in a stunning quality.  Faceit provides a direct interface for live capturing and recording the facial expressions in the Blender viewport.  

![tile](img/mocap/hallway/tile.jpg)  

Hallway Tile runs on Windows, Mac and Linux computers. **You merely need a regular webcam.** Of course, the quality of the captured data depends on the resolution and framerate of the webcam.  

[Head to Hallway right now and start capturing your facial expressions.](https://joinhallway.com/){target=_blank}  

!!!tip "Import to Faceit"  
    - Record live via [OSC](mocap_live.md#hallway-tile).  

<!-- ![tile](https://hallway-public.nyc3.cdn.digitaloceanspaces.com/assets/tok_face_occ.mp4)   -->
___

## Face Landmark Link

[Face Landmark Link](https://github.com/Qaanaaq/Face_Landmark_Link/) is a user built, free and open source application that allows to record ARKit data from video sources and also live from a webcam feed. The software uses Googles media pipe to extract the animation data.  

![fll](https://camo.githubusercontent.com/f9d5397cb9bbb3e04bb5df6ddcdf6255c689f1dfb1b9b163b9bf96c679ae2a23/68747470733a2f2f692e696d6775722e636f6d2f73585948656d6b2e706e67)
*ARKit data from any 2D video source. Recorded or realtime.*  

!!!tip "Import to Faceit"  
    The Face Landmark Link app can output CSV files compatible with the [Live Link Face Importer](mocap_importers.md#live-link-face) and also stream live link face data to in [real-time](mocap_live.md#live-link-face). A tutorial can be found [here](https://vimeo.com/878474114)  



___

## Nvidia Audio2Face

[Nvidia's Audio2Face](https://www.nvidia.com/en-us/omniverse/apps/audio2face/){target=_blank} is a combination of AI based technologies that generates facial motion and lip sync that is derived entirely from an audio source. It can be used at runtime or to generate facial animation for more traditional content creation pipelines. The resulting blendshape weights can be exported to .json files which can in turn be imported into Blender via Faceit. Audio2Face is part of the [Nvidia Omniverse Platform](https://www.nvidia.com/en-us/omniverse/){target=_blank}.  

!!!tip "Import to Faceit"  
    * [Import A2F weights via Json format](mocap_importers.md#audio2face).

!!!tip
    The new Audio2Face version comes with ARKit solvers as well, so you can now also import A2F data directly to the Faceit control rig.  
    ![a2f](img/mocap/a2f-00.png)


### Hardware

>!!!warning "RTX only"
    For now, Audio2Face requires the usage of an Nvidia RTX video card and a Windows operating system. See [**this**](https://www.nvidia.com/en-us/omniverse/apps/audio2face/#system-requirements){target=_blank} page for details on hardware requirements.  
___

## General Retargeting (FBX)  

For convenience, Faceit also provides a set of tools that allow to retarget any type of shape key animation to other characters. Read more [here](fbx_retargeting.md).  

!!!tip "Import to Faceit"  
    If you want to import blendshape animation data from other sources, it's often the easiest to import an FBX with the animation. Faceit comes with the convenient [Shape Key Retargeter](fbx_retargeting.md), so you can quickly rename the data paths in the action itself to match your character model.  
<!-- 
[^1]: [In 2015 Apple acquired the motion capture company **FaceShift**](https://www.bbc.com/news/technology-34920548){target=_blank}. Not long after that, Apple released the facial performance capture solution that is now widely used throughout the industry.   -->