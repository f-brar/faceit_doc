# Target Shapes

 For each source expression in the ARKit or Audio2Face recordings, you can specify one or multiple target shape keys. Faceit will browse this list upon executing any motion capture operator to decide which shape keys should be animated. Faceit provides a **target shapes list** for both the 52 ARKit shape keys as well as the 46 Audio2Face shape keys. Before any of the Mocap functionalities can be used, the respective list has to be populated. It doesn't matter if you created the required expressions with Faceit or by other means.  

???tip "How to Create the Expressions?"  
    If you haven't already created the shape keys, you will need to do that first. You can either create them manually or use the [Faceit Create](getting_started.md#so-how-do-i-start) workflow. Check the [Expression Reference](expression_reference.md) section for details on the individual sets.  

## Assign

>### Automatic

>The **Find Target Shapes** operator will search all available shape keys on all registered Objects to find the best matching Target Shape for each ARKit Source Shape. A **Similarity Ratio** (default 1.0) can be used for fuzzy name detection. A **lower value** ratio means that the operator will also find Shape names with less similarity. This is useful when the shape keys on your character are named remotely similar to the ARKit names.  

>![auto_assign](img/mocap/target_shapes/auto_assign_a2f_00.gif)  
>*Automatically find the ARKit and Audio2Face target shape keys.*

>![prefix](img/mocap/target_shapes/find_prefix_00.gif)  
>*Specify a prefix or suffix that should be ignored in name comparison.*  

>### Manual

>If the automatic assignment of target shapes fails or if you want to add multiple target shape keys to one source expression, you can manually assign the target shapes.  
>![assign_manually](img/mocap/target_shapes/assign_00.gif)  
>*You can assign all target shape keys manually, by using the + operator.*  

>![assign_manually](img/mocap/target_shapes/assign_multiple_00.gif)  
>*You can assign multiple target shapes or remove them by clicking on the target shape drop-down.*  

??? tip "Sync Selection"
    ![sync_selection](img/mocap/target_shapes/sync_selection.jpg)
    *The **Sync Selection** option will automatically synchronize the selected expression with the active shape key in the properties editor. Enable **Show Only Active** to preview the target shape keys in isolation (set to 1.0).*  

## Presets (Export/Import)

The **Export** and **Import** operators allow to save and re-use the Target Shape lists. This is especially useful, if you have a specific expression set on multiple characters. Popular presets will be implemented into Faceit and can be loaded automatically.  

![sec](img/mocap/arkit_cc3.gif)  
*Load Presets or export/import your own FACS retargeting schemes. Red list entries can't be found on registered objects.*  

## Face Regions

Each source shape is associated with a specific facial region. The regions can be used to filter animation curves upon import. This is useful if you want to animate only the mouth or eyes. You can change the associated face region for each expression.  

![sec](img/mocap/target_shapes/filter_regions_00.jpg)  
*Quickly filter the target shape list by assigned regions.*  

![sec](img/mocap/target_shapes/filter_regions_01.jpg)  
*Change the assigned regions to individual needs.*  


## List Order and Naming (ARKit / Face Cap)

For convenience you can change the display names of the Source Shapes to either standart ARKit ordering/names or Face Cap ordering/names. This has no effect on the functionality of Faceit, but can be useful when exporting to game engines that require a specific ordering for the shape keys.  
!!!warning ""
    The results may be unexpected if you specified multiple target shapes per source shape or if the target shapes are distributed over several objects.   

![sec](img/mocap/target_shapes/arkit_shapes_00.gif)  
*Reorder and Rename Target shape keys quickly!*  