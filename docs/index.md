![Faceit](img/FaceIt0801_FaceIt_LogoType_LogoIcon_WithColor.png)

# Welcome to the Faceit Documentation

**Faceit** is a Blender Add-on that assists you in creating complex facial expressions for arbitrary 3D characters.  

An intuitive, semi-automatic and non-destructive workflow guides you through the creation of facial shape keys that are perfectly adapted to your 3D model's topology and morphology, whether it’s a photorealistic human model or a cartoonish character. You maintain full artistic control while saving a ton of time and energy.  

<!-- [Deprecated: Find the old documentation for version 1.3.3 and above here.](https://faceit-doc.readthedocs.io/en/v1/){target=_blank}

[TOC]   -->
!!! tip "Getting Started"
    Read the [Getting Started](getting_started.md#getting-started) section, before browsing the rest of the documentation!  

![all_expr](img/rain_mocap_00.gif)  
*Rain Character - shape keys and capture done with Faceit (Rain Rig © Blender Foundation)*  

## Versions

### Faceit 1.8 - Dedicated ARKit Toolset

Faceit 1.8 is the base tool that is packed with all features that you might need for ARKit motion capture and many more.

* Rig your characters face in an easy and intuitive process.  
* Semi-automatically create the **52 ARKit shape keys** and give them personality.  
* Create a full fledged slider based [**control rig**](control_rig.md) to animate the shape keys.  
* Import recorded [motion capture animations](mocap_general.md) to the control rig or directly to the shape keys!  
* [Animate and record live](mocap_general.md) in the viewport, without any additional tools required.  
* Join the face rig to any body rig automatically.
* Many more additional tools & utilities that make facial animation a fun experience.  
* As a bonus, Faceit 1.8 includes the import and retargeting tools for Audio2Face motions.  

### Faceit 2.3 - Unlimited Expressions

Faceit 2.3 extends the functionality of version 1.8 by a range advanced features. It is dedicated to professionals who need to create shape keys beyond the 52 ARKit expressions. It allows to create, export and import **custom expressions** and comes with a number of expression presets, that will be expanded over time. You can share your own expression presets as **content packs**. Faceit 2.3 allows to use any kind of custom rig.

* [**Create, import and export custom expressions**](create-expressions.md#create-custom-expressions)! Reorder, add or remove expressions by the press of a button!  
* Choose from a number of [**presets**](create-expressions.md#load-expression-presets) shipped with Faceit. *(Presets will be expanded over time)*  
* Tongue movements, visemes, FACS, or whatever it is that you always missed among Faceit expressions. You can now create it!  
* Use your own custom rigs.  

### Faceit 2.3 - Studio (2 - 9 Seats)

The Studio version is dedicated to studios with multiple users. It is also a means to support development with bigger funds. By purchasing the Studio version you get access to Faceit version 2.3 with an identical feature set.
