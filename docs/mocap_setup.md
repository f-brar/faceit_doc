# Motion Capture - Setup

Before you can start using any of the motion capture operators in the Mocap tab, you will need to register the **target objects** (character model) and **target expressions** (shape keys). This step is necessary to specify which objects and expressions should be animated. If you created the expressions with Faceit, it is likely that everything is already setup correctly.  

!!!note 
    If you haven't created any shape keys yet, you can do so by following the [Faceit Create workflow](getting_started.md#so-how-do-i-start)

## Target Objects

In this step you need to specify which objects should be affected by the motion capture operators. The process is identical to the [**Register Geometry**](setup.md#register-geometry) process in the expression creation workflow. You can obviously only animate those object that are equipped with the necessary shape keys.  

![main](img/setup/register_objects_00.gif)  
*Register all objects that you want to animate.*  

## Target Expressions

The shape keys do not need to match the source expressions exactly. Faceit provides a powerful **re-targeting tool**, called [**Target Shapes**](target_shapes.md) list.  For each source expression in the ARKit or Audio2Face recordings, you can specify one or multiple target shape keys. Faceit will browse this list upon executing any motion capture operator to decide which shape keys should be animated.  

???+note "Expression Sets"
    Depending on which motion capture workflow you are using (**ARKit**, **Audio2Face**), you will need to create the respective set of shape keys for your character. It doesn't matter if you create the required expressions with Faceit or by other means. Take a look at the list below to learn which mocap apps require which expression set.  

    | Mocap Method      | Expression Set                          |
    | ----------- | ------------------------------------ |
    | `Face Cap`       | [ARKit](expression_reference.md#arkit)  |
    | `Live Link Face` | [ARKit](expression_reference.md#arkit) |
    | `Hallway Tile`   | [ARKit](expression_reference.md#arkit) |
    | `iFacialMocap`   | [ARKit](expression_reference.md#arkit) |
    | `Audio2Face`     | [Audio2Face](expression_reference.md#audio2face) or [ARKit](expression_reference.md#arkit) |  

## Head Tracking

All motion capture operators (except for audio2face) provide the option to animate your characters head. Registering the head object for animation is as easy as populating the **Head Object** field in the mocap tab. If the registered object is of type armature (a rig), you can optionally specify a bone. Usually, animating bones is preferable over animating objects, because it allows for smooth deformations.

![head_target](img/mocap/head-setup.png)  
*Quickly assign the head bone of your character before loading mocap data.*

## Eye Tracking

Eye rotation can be animated by shape keys (*eyeLookDown, eyeLookIn, ...*) and/or bone rotation. Most often either will be sufficient, but sometimes a combination is needed. For example, when the eyeballs should be rotated via bones and the eyelids should follow that rotation via shape keys. This depends on the individual character model.

![eyes](img/mocap/eye-setup.png)  
*Similar to the head bone, you can quickly assign eyebones as targets for imported rotation.*  

!!!warning "Shape Keys can't rotate!"
    Note, that while shape keys can convincingly animate the eye rotation, it's not a real rotation. Shape keys can only store linear translation data. The rotation is always a combination of linear movement, which leads to shearing. The effect is often only visible when seeing the whole eyeball.    
    ![eyes](img/mocap/eye-shape-rotation.gif)  
    *Shape Key Fake rotation effect.*  
