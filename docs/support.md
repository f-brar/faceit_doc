# Support

[TOC]  

## Bug Reports
* Please share the version numbers of both **Blender** and the **Faceit** add-on *(e.g. 2.9.3, 1.5.3)*.
* **what** is the problem and **how** can I reproduce it?
* If possible, please share the **Blendfile** *(yourfilename.blend)*. That makes everything way easier!

## Get in Contact  

If you want to **report a bug** or **encounter problems** with Faceit, please reach out over one of the channels mentioned below:

!!! Tip "Discord Community"
    Join the new **Faceit Discord Server**! This is the best place to ask questions, share tips and tricks or even report bugs.  
    [Join Faceit Discord!](https://t.co/74OwtD4YeZ?amp=1){target=_blank}


!!! Warning "Blendermarket"
    As a **Blendermarket customer** you can [Ask a Question](https://blendermarket.com/products/faceit){target=_blank}.  
    ![Ask](img/other/ask_a_question.jpg){width=20%}{position=center}  
    *Blendermarket - Ask a Question button*

!!! Note "Gumroad"
    As a **Gumroad user** you can write an Email at [fynn.braren@posteo.de](mailto:fynn.braren@posteo.de)



