# Rigging

## Generate Rig

The **Generate Rig** operator generates the **Faceit Rig** (the Rigify armature that will deform the geometry).  
Most bones are placed at exact [**landmark positions**](landmarks.md), while some are interpolated between multiple points and some are calculated from the [**assigned vertex groups**](setup.md#2-assign-vertex-groups).  

<video controls loop autoplay width="100%">
 <source src="../img/rigging/rig-00.mp4" type="video/mp4">  
</video> 
*The Armature is generated based on the landmarks placement. That is why landmarks are crucial to get right!*  

<video controls loop autoplay width="100%">
 <source src="../img/rigging/rig-01.mp4" type="video/mp4">  
</video> 
*Since version 2.3 it's also possible to generate the new Rigify face rig. You can optionally adapt the meta rig before generating the final armature.*    

### Back to Landmarks

If the generated bone placement is not accurate you can always go back via the **Back to Landmarks** operator. In case the geometry of eyes or teeth or tongue seems to be misplaced, then you will have to [**edit the assigned Vertex Groups**.](setup.md#2-assign-vertex-groups) or add [**Locator Empties**](landmarks.md#create-locator-empties) before re-generating the rig.  

!!!tip "Non-Destructive"
    **Save** the **Bind Weights**, **Expressions** and **corrective shape keys** when going back. Faceit will store them and reapply them on re-generating the Faceit Rig

![rig](img/rigging/back_to_landmarks_00.gif)  
*Easily go back to editing the landmarks and regenerate the Rig. Keep expressions, shape keys and bind weights if needed.*

## Bind Weights

The **Bind** operator binds all registered object(s) to the Faceit Rig! All existing data will be preserved, that means that binding is non-destructive too (Vertex Groups, Modifiers, shape keys and Drivers etc. stay untouched). The more care was invested in the Setup and landmarking process, the better the automatic weighting results will be! Take a look at the [**Bound Geometry**](#bound-geometry) section below for details on how the bind operator works.  

???+ tip "Advanced Settings"
    You can enable and disable the individual steps in the bind operator. You should leave the advanced settings untouched, if you don't have a reason to belief that the weighting results will benefit from changing them.  

### Bound Geometry

* The [**main face geometry**](setup.md#main-group) will be bound **automatically** and Faceit will usually do a good job on **removing weights from the skull and the neck.** If parts of the geometry are affected by weights that you do not want, you can assign them to the [rigid group](setup.md#rigid-group) and re-bind.  
* The [**eyes geometry**](setup.md#eyeball-groups-left-and-right) will be weighted **only** to the bone *'DEF_eye.L'* or *'DEF_eye.R'*.  
* The [**teeth geometry**](setup.md#teeth-groups-upper-and-lower) will be weighted **only** to the bone *'DEF_teeth.B'* or *'DEF-teeth.T'*.  
* The [**tongue geometry**](setup.md#tongue-group) will be weighted automatically to the *'DEF_tongue\*'* bones.  
* There are two options for the [**other geometries**](setup.md#23-secondary-groups) (facial hair etc.). Based on the chosen Bind Method in the advanced settings of the Bind operator, they will be weighted based on the main weights (via **Mesh Data Transfer**) or via automatic weights (default).  

!!!tip "Object Mode"  
    You need to be in **Object Mode** for binding to work.  

![rig](img/rigging/bind_00.gif)  
*The bind operator will bind all geometry except for facial hair and similar optional geometries. You can modify the resulting weighting by a number of options.*  

!!! danger "Known Issues"
    It is important to present clean geometry. If you run into problems while binding, there are a few things you can try to auto-weight afterall. If it still fails you can bind the rig manually.

    * Remove duplicate vertices. **Edit Mode > Mesh > Clean Up > Merge by Distance**
    * Higher Scale-factor in the bind settings.  


## Bind Results

I would advice to go ahead and create the expressions, in order to test the deformations. It is the quickest way to check the weights for errors. If you are unhappy with the weighting or rigging results, you can do multiple things!

* Go back to landmarks and refine them.  
* Change the advanced settings in the bind operator.  
* Manually paint the weights (only for advanced riggers).  
* Last but not least, the main goal of the Faceit process is usually to create shape keys and not a fully functional rig, so it is often the best method to fix individual expressions via [non-destructive sculpting](expressions.md#non-destructive-sculpting)

## Corrective Smooth

This is **optional**, but can enhance the quality of deformations for some characters! Select an object that has been bound to the Faceit Armature and click the **Smooth Corrective Modifier** button. This will create a SmoothCorrect modifier in the stack of your object. You can play around with the settings to change the effect.  

![smooth](img/smooth_correct_01.gif)