# Expressions

After the rig is in place and properly bound to your characters geometry, you can now start with the most important step in the process. **Creating and adapting the individual facial expressions**.  

!!! warning "Faceit - Version 2.3 Only"
    **Custom Expressions**, as well as **Exporting and Importing** of **Expression Presets**, is available in **version 2.3 only**. If you are a user of **version 1.8**, Faceit will automatically load the **ARKit expressions** on your character. You can skip directly to the [**Expression Editing**](expressions.md) section of this documentation.  

You can create custom expressions or choose from a number of expression presets. After loading you can adapt them to your characters morphology with a range of [**powerful tools**](expressions.md)!  


## Load Expression Presets

A number of Expression presets are shipped with Faceit. You can load any Faceit preset with the **Load Faceit Expressions** operator! Presets include the 52 ARKit expressions that are required for the motion capture functionality and the control rig. After loading a preset you can adapt each expression to your character, by [**posing or sculpting**](expressions.md#edit-expressions).  

![animate0](img/expressions/load-expressions-00.gif)   
*Load Faceit Expression Presets quickly!*  

<!-- !!! tip "Existing Face Rig" -->
### Existing Face Rig  

In case you registered an [existing face rig](setup.md#use-existing-face-rig), Faceit you can load empty expressions from the presets. The poses will have to be created manually (all the workflows on this page will work). Using the [ARKit reference model](#arkit-reference), or alternatively searching for other reference models is highly recommended when creating the expressions from scratch.  

<video controls loop autoplay width="100%">
 <source src="../img/expressions/any-rig-expressions.mp4" type="video/mp4">  
</video>
*Load empty expressions to custom rigs and quickly pose them manually.*  

!!! tip "Control Bones"  
    When using custom rigs it's important that you assign the correct control bones beforehand. This ensure that the rest pose keyframes can be added automatically.  
    ![c](img/expressions/control-bones.png)  

### ARKit Reference

When loading the ARKit expressions, you can choose to load an animated reference model.  

<video controls loop autoplay width="100%">
 <source src="../img/expressions/arkit-reference.mp4" type="video/mp4">  
</video>
*Loading an ARKit reference model can be extremely helpful for finetuning the expressions.*  


## Create Custom Expressions

Create Custom Expressions by the click of a button. They will work similar to any other expression. Of course, newly created custom expressions need to be posed from scratch. You should choose a descriptive name for your expression in this step. The name will be the same as the final Shape Key baked added to your meshes.  

!!!tip "Auto Mirror Custom Expressions"
    Faceit will offer to automatically mirror your expression if you add a suffix of *_L*, *_R* or *left*, *right* to the expression name.  

![animate0](img/expressions/custom_expressions_00.gif)   
*Create Custom Expressions to be baked as new shape keys*

## Export Expression Sets

Expression sets can be **exported and shared** at any point in the creation process. You can use this feature to share custom shape keys with other characters within in your production or **sell** your custom expression sets as **Faceit content packs**! Exported expression sets are saved with a *.face* file extension. Exported expressions will contain transform keys for all pose bones on layers 1,2,3 and respectively the keys for all pose bone constraints.  

![animate0](img/expressions/export_00.jpg)  

*Export Expression Data to .face format.*  
## Import Expression Sets

You can import expression sets, created by you or by other users. During Import you can choose to scale all expressions to the dimensions of your character. All contained expressions will be applied to your character and appended to the expression list.  

![animate0](img/expressions/import_00.jpg)   
*Import Expression Data from .face files.*  

!!!note "Options (Import Expression Sets)"
    The options for loading custom expression sets are identical to the [**Load Faceit Expressions** options](#load-expression-presets)

## Delete Expressions
You can remove single expressions or clear the entire list in one click, by choosing **Clear Expressions** from the drop down menu.  

![animate0](img/expressions/reset_00.gif)  
*Clear all Expressions quickly.*   



??? tip "New Expression Sets"
    Faceit expression presets will be expanded with coming updates. If you want to share your expression set with the Faceit community you can send it to me and I will consider to include it as a preset. You are free to share your presets with community members for free or sell them as a content pack.  