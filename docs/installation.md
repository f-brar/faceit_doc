# Installation

[TOC]  
___
**Edit > Preferences > Add-ons > Install**  
___
Faceit comes in a **.Zip** folder. You do not have to unpack it. Simply click the `Install` button and browse the filepath to wherever you saved your Faceit copy. Afterwards you will need to enable the Add-on by clicking the enable checkbox. See the images attached below.
___
!!!tip "How to Update"
    To update, simply follow the installation steps and overwrite the current installation. **Restart** Blender for an **Update** to take effect.


![install1](img/install_addon_0.jpg)  
*Install or Update the Add-on*  

![install2](img/install_addon_2.jpg)
*Enable the Add-on*  

![install3](img/install_addon_1.jpg)
*Find the Add-on in 3D view (N-Panel)*
