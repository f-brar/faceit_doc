# Bake

[TOC]  

## Bake shape keys  

The operator **Bake shape keys** will bake the deformation of all [**Faceit expressions**](expressions.md) into [**individual shape keys**](getting_started.md#what-are-shape-keys).  
Next to the deformation from the [**FaceitRig Armature**](rigging.md) and [**Corrective shape keys**](expressions.md#sculpt-expressions), the bake operator does allow a range of deformation sources, including non-generative modifiers, shape keys, other Armatures and transformations.  

![bake](img/bake/bake-01.gif)  
*Non-destructively bake all expressions to shape keys.*  

### Bake Modifiers 

You can activate all deform modifiers in the stack of your registered objects for baking. All active bake modifiers will be baked to the final shape keys.  

![bake](img/bake/bake-mods.png)  
*Activate all modifiers that contribute to the relevant deformation.*  


!!!tip "Non-Destructive (Back To Rigging)"
    The Bake operator is completely non-destructive. You can [bake the expressions into shape keys](#bake-shape-keys) test them in motion and return to [editing expressions or rig](#back-to-rigging) by the click of a button.  
    --> [**Back To Rigging**](#back-to-rigging)

![non-d](img/rain_all_expressions_00.gif)  
*You can generate a test action to see the shape keys on all objects. You can still use the [expression list](expressions.md#expression-list).*

## Back to Rigging
The [**Bake**](#bake) process is **non-destructive**, which means that if you are unhappy with your resulting shape keys, you can still return to [**rigging**](rigging.md) or [**edit the expressions**](expressions.md) by the press of a button.  

The **Back to Rigging** operator will restore the **FaceitRig**, the **Bind Weights**, the **Expressions** and the **Corrective shape keys**!   

![non-d](img/bake/back.gif)  
*Quickly and non-destructively iterate between baked shape keys and rigging/animating the expressions.*  

!!!warning "WARNING"
    Going back will remove the generated shape keys and clean the ARKit Shapes list. You can quickly regenerate them by baking again.  
