# Expressions

[TOC]  


After the rig is in place and properly bound to your characters geometry, you can now start with the most important step in this process. **Creating and adapting the individual facial expressions**.  
     

## Expression List
This list holds references to all loaded expressions. Clicking through the list, allows you to jump straight to the respective timeline position and start [**editing the expression**](#edit-expressions)   
![animate0](img/expressions/expressions_list_00.gif)  
*Quickly go to the keyframe of an expression pose, by clicking on the entry in the list or using the Up/Down arrow keys.*   

## Edit Expressions
Expressions can be edited, adapted or created by multiple methods. All of these methods are **non-destructive**!  

* **Pose** Expressions (pose control bones)  
* **Sculpt** Expressions (corrective sculpting on any registered mesh)  
* **Amplify** Expressions (amplify animated poses up or down)  
* **Mirror** Expressions (automatically mirror left to right sides)  
* **Reset** Expressions (allows non-destructive edits)  

![animate0](img/expressions/mouthClose.gif)  
*From 1.5.x onwards the mouthClose Expression is finally editable.*  

!!!tip "Combine Methods"
    It is most effective to combine both sculpting and posing! Create a base expression by posing the bones or loading a preset and add details and character by sculpting on top.


## Pose Expressions

To edit an expression you can simply go into **Pose Mode**, pose the expression to your liking by **translating**, **rotating** and **scaling** the control bones and insert keyframes. You can safely overwrite any expression, because Faceit will store the original expression for you. [See here how to restore a keyframe.](expressions.md#reset-expression)  

![animate0](img/expressions/edit_pose_00.gif)  
*You can pose expressions quickly by moving the pose bones. Enable Auto Mirror X and Auto Keying for an effective and intuitive posing experience.*

### Reset Expression Pose
If you are unhappy with any applied edits, you can always return to the originally generated expressions, by the **Reset Expression** operator. New custom expressions will be reset to the rest pose. Loaded or imported expression sets will be reset to their original values.  

![animate0](img/expressions/reset_pose_01.gif)  
*Messed Up!? No Problem! Quickly reset Poses to the initial preset value.*  

![animate0](img/expressions/reset_pose.jpg)  
*You can also reset all expressions at once*  

## Non-destructive Sculpting

By selecting a registered mesh and clicking the small **Sculpt** Icon next to each expression in the expression list, you can go directly into Sculpt Mode and sculpt on top of the base pose. You can use all the power of Blenders sculpting tools to bring **detail** to your characters facial expression or correct **artifacts** induced by bad bind weights.  

Corrective Sculpting is fully non-destructive! All Deformation is stored in temporary shape keys on the mesh data. Upon baking, the corrective shape keys will be muted. If you need to edit expressions and go back to rigging, the Sculpt layer will be restored too.  

![animate0](img/expressions/sculpt_00.gif)  
*You can sculpt on top of any registered mesh. All Sculpting will be baked into the shape keys.*

!!!tip "Corrective Sculpting"
    To start sculpting on a mesh you need to be in **Object Mode** and select the specific object.  
    Make sure you enabled the **Use Corrective shape keys** option!  
    If you want to remove a sculpting, you can click the little cross icon next to the sculpt icon in the list. If you want to hide sculpt effects, you can simply disable **Use Corrective shape keys** for a moment.  
    ![animate0](img/expressions/sculpt_00.jpg)  



## Amplify Expressions
Quickly amplify the effect of the selected expression by scaling the respective curves. Changes to the value will take effect in realtime! This might be slow in complex scenes.  

![animate0](img/expressions/amplify_00.gif)  
*Quickly Amplify an expression. All posed bones or only selected.*

!!!note "Amplify All"
    You can also quickly amplify all expressions in the list:
    ![animate0](img/expressions/amplify_all_00.jpg)  

## Mirror Expressions
You can mirror edits that you applied to a micro expression on one side to the other side by the clicking the **Mirror Expression** button. This operator will work on any expression that has a *Left* or *Right* suffix and a corresponding mirror expression.  

Both the **bone pose** and the **corrective sculpting** can be mirrored. Depending on your geometry you might have to change the corrective shape key mirror settings.  

![animate0](img/expressions/mirror_00.gif)  
*Mirror any Expression from left to right side. Expressions that effect both sides of the face will be mirrored automatically.*  