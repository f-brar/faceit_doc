# Getting Started
Face rigging, animation and motion capture is still considered one of the most difficult disciplines of computer graphics and even with Faceit doing most of the heavy lifting, you will most probably find yourself confronted with some new terms and workflows. While the learning curve may be steep in the beginning, I am confident that you will soon get the hang of using Faceit and continue producing wonderful art and even better animations. The whole rigging, creation and animation process is fully non-destructive! You can always go back and tweak any step in the pipeline, without ever risking to loose anything! This documentation is supposed to serve as an in-depth guide for all features and workflows in Faceit.  
## So How Do I Start?
Well, it depends on your individual project and what you want to achieve with Faceit. Read on below for a few possible use-cases.  
>**Scenario 1 - Create Expressions** Do you have a static character model and want to prepare it for motion capture animation? You are in for the full experience! Start in chapter [Prepare Geometry](geometry.md) and follow the process all the way down to [Bake](bake.md). Afterwards, you can head straight to the [Mocap tab](mocap_general.md) or [generate the ARKit Control Rig](control_rig.md) for better control over your animations.  
___
>**Scenario 2 - Motion Capture Animation for CC4 character** If your model already has a FACS based shape key set, then you can skip the rigging and animation process and head directly to the motion capture workflows. Quickly [register the objects](setup.md#1-register-geometry) that you want to animate in setup tab and [assign the shape keys in the target shapes list](target_shapes.md)  
___
>**Scenario 3 - Manual Animation** The face rig generated with Faceit has the same structure like the Rigify face rig and is very capable of regular keyframe animation. You do not need to create facial shape keys and use motion capture tools in order to animate your character. You will also find a handy utility in the Rig tab that allows to [join the face rig to an existing body rig](utils.md#join-to-body-destructive), for a better animation experience.  
!!! tip "Workflows"
    Depending on your individual goal, you can choose to display only relevant workflow tabs.  
    ![main](img/setup/workflow_ui.gif)  
### How does it Work?
The image below is supposed to give you a broad overview of the non-destructive workflow of Faceit. I tried to give some insight into the rigging and animation process in the individual chapters throughout this documentation.  

![flow_chart_00](img/Faceit-Process-FlowChart-05.jpg)  
*You can go back and forth between all steps with two-sided arrows, without loosing your progress.*

### What are shape keys?
Shape keys are used to deform objects into new shapes for animation. The technique allows a single mesh to deform to numerous pre-defined shapes or even any number of combinations of these shapes. The **Basis** Shape, is the default shape of the mesh (for example a neutral expression on a characters face). The **Key** Shapes or **targets**, are exact copies of the base mesh that are deformed into another pose. For example, the same characters face, smiling or frowning. Each facial expression is stored in another **Key** Shape. Shape keys store linear translations for all vertex positions on a mesh. This can be a limitation if you want to produce curve motions. It's common to work around this by creating corrective shape keys that add curvature to an otherwise straight motion. In general, it's always the combination of multiple shape keys that produce natural looking motion (this is especially true for facial animation). Shape Keys are stored on a per-object basis. This means that, if you have multiple objects and you want to change a specific shape key on all of them, you need to do it for each object individually. The expression workflow in Faceit allows to quickly affect the expression on all objects.  

##### <a name="terminology"></a> Terminology - Shape keys are also referred to as 
> * **Blendshapes**
> * **Morphs**
> * **Morphtargets**
> * **Sliders**
> * just **Shapes**
> * and probably many more...  


!!!note "Animate Shape Keys"
    The [Shape Key Control Rig](control_rig.md) is very handy for animating shape keys, because instead of changing the individual shape key values with tiny UI sliders, you can now intuitively animate bones and use animation layers for easy and non-destructive changes to your animations.  

## Disclaimer
Although I did test Faceit on versatile characters with varying morphology and topology, I can not guarantee perfect results for every single character. Depending on the character and the requirements for your project, you might have to invest more time in fixing artifacts. Problems might occur on individual characters during binding/weighting or even with the generated expressions. I will try to present solutions for common error sources in this documentation, but please don't hesitate to [contact me](support.md) for advice!  

