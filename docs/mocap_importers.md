# Motion Capture - Importers

Import captured animation data from various apps into Blender actions and onto the registered character or the active control rig. The import operators are stream-lined and the import process is identical for all applications. You can import animation data from [Face Cap](#face-cap), [Live Link Face](#live-link-face) and [Audio2Face](#audio2face). Read on below to learn how to import animation data from each of these applications.  

1. **Load File** - Specify the file path of the recorded data.  
2. **Load Audio** (Optional) - Specify the file path of the recorded audio.  
3. **Import** - Hit Import to start the import process.  

## Import Settings

Faceit allows to import either head rotation and location or shape key animation data. Depending on the application you are using and your active character, you can import either one or both of these.  

>### Animate Shapes

>Toggle off `Animate Shapes` if you want to import only head animation data. This is not available upon audio2face imports, because the audio2face app only exports shape key data.  

>### Filter Face Regions

>The face region filter can be used to specify which animation data should be imported. For example, disable all but `Eyes` to only import animation data for the eye shape keys.  
>!!!tip ""
    Face regions can be changed in the [target shapes panel](target_shapes.md#face-regions).
>![face_region_filter](img/mocap/importers/region_filter_00.jpg)  
*Use the face region filter to specify specific facial regions that should be animated.*  

>### Smooth Animation  

>You can smooth the imported animation data. Each motion types (shapes, head, eyes) can have it's own smooth factor (the smoothing amount).  

>![smooth](img/mocap/smooth-settings.png)

>### Action Settings

>Choose an action from the dropdown or create a new one, right in the import operator options. If no action is selected, a new action will be created. (Works for head target and shape keys)  

>![action_settings](img/mocap/importers/action_settings.jpg)  
*Create a new Head Action upon import.*  

>![action_settings](img/mocap/importers/action_settings_00.jpg)  
*Choose an action from the dropdown*  

>#### Overwrite Method

>Choose how to handle existing animation data. Either `Replace` the entire action, or `Mix` the new animation with existing keyframes.  


>| Method      | Behaviour                          |
| ----------- | ------------------------------------ |
| `Replace` | Re-create the entire action.  |
| `Mix` | Overwrite keyframes in the frame range. |


>#### Start Frame

>Specify the start frame of the imported animation. Use it together with the mix method to add the new animation data to the end of an existing animation, or overwrite parts of an existing animation.  
>![action_settings](img/mocap/importers/action_settings_01.jpg)  
*Choose `Mix` method and a start frame to merge the new data into an existing action.*  

>### Head Animation

>Enable Rotation and/or Location to import head animation data.  
> The `Head Object` and `Head Bone` fields are identical to the ones in the [Head Setup panel](mocap_setup.md#head-tracking)

## Face Cap

The text format contains the raw ARKit animation data. With Faceit you can import the data and retarget it to your character.  

!!!tip ""
    Face Cap records animation data for head rotation and location as well as ARKit shape key data.  
![txt01](img/mocap/importers/import_txt_00.jpg)  
*Import Settings for a Face Cap text file.*  
![txt02](img/mocap/facecap/panel_settings.jpg)  
*Enable the txt export option in settings in order to automatically export text files.*  

## Live Link Face

The CSV format contains the raw ARKit animation data. With Faceit you can import the data and retarget it to your character. CSV files are exported automatically with each recording. You can find them on your iOS device  

!!!tip ""
    Live Link Face records animation data for head rotation (no location) as well as ARKit shape key data.  

![txt01](img/mocap/importers/import_csv_00.jpg)  
*Live Link Face csv import.*  

## Audio2Face

Since 2023 Audio2Face does also offer ARKit presets (52 ARKit shape keys), so it's now also compatible with the control rig. You can still use the old FACS 46 expression set, if you prefer that. The framerate option is determined automatically. You don't need to change it.  

!!!tip "Export ARKit motion"  
    1. Choose an example solver from the bundled presets:  
    ![a2f](img/mocap/a2f-00.png)  
    2. Load your audio file and test the result. Audio2Face got a lot of settings for tweaking the results, as well as different AI models. See the official documentation for more information. Note, if your model has the required target shape keys, the exported motion should work immediately. There is no need to import your model to Audio2Face.  

        * [Audio2Face Documentation](https://docs.omniverse.nvidia.com/audio2face/latest/index.html)  
        * [Audio2Face Video Tutorials](https://www.youtube.com/playlist?list=PL3jK4xNnlCVfIJU10iQ_dd8J_5T_e0YTJ)  
        
    3. Export a Json clip with the blendshape weights.  
    ![a2f](img/mocap/export.png)
    4. Import via the Faceit json importer.  
    ![a2f](img/mocap/a2f-import.png)  
    *The A2F importer has no options for eye and head rotation.*  