# Export

[TOC]  

There are several formats that allow to export your character meshes with shape keys, rigs and animation data. Which format is the right one depends on the individual project. Check the documentation of your target package for specific requirements. Remember that shape keys is a Blender specific term [(See the terminology section for equivalents)](getting_started.md#what-are-shape-keys).  

!!!warning "Export Limitations"
    The [Faceit Control Rig](control_rig.md) can't be exported. The driver set up will only work in Blender. Make sure to bake the animation to shape keys if you want to use it in other software.  

    The [Rigify face rig](rigging.md) generated with Faceit can be exported to other software that support bone animation (most game engines do). However, the constraint and driver setup will only work in Blender. That means, the control and mechanism bones will be useless in other software. Animations need to baked to the deform bones (the exporters can handle this process).

## glTF

>**File > Export > glTF 2.0 (.glb/.gltf)**

>glTF™ (GL Transmission Format) is used for transmission and loading of 3D models in web and native applications. glTF reduces the size of 3D models and the runtime processing needed to unpack and render those models. This format is commonly used on the web, and has support in various 3D engines such as Unity3D, Unreal Engine, and Godot.

>[Read more in the Blender Manual](https://docs.blender.org/manual/en/2.80/addons/io_scene_gltf2.html)  

## Universal Scene Description (USD)

>**File > Export > Universal Scene Description (.usd)**

>[Read more in the Blender Manual](https://docs.blender.org/manual/en/latest/files/import_export/usd.html)

>!!!warning "4.1+"
    Skeletal animation and Shape Keys are only supported in Blender 4.1 and above. [Read more in the 4.1 Change Log](https://developer.blender.org/docs/release_notes/4.1/pipeline_assets_io/#usd).

## FBX

>**File > Export > FBX (.fbx)**  

>This format is mainly use for interchanging character animations between applications and is supported by applications such as Cinema4D, Maya, Autodesk 3ds Max, Wings3D and engines such as Unity3D, Unreal Engine 3/UDK and Unreal Engine.

>[Read more in the Blender Manual](https://docs.blender.org/manual/en/2.80/addons/io_scene_fbx.html){target=_blank}  

>!!!warning "Limited FBX support"
    Due to the FBX licensing model, Blender does not include the official Autodesk FBX SDK. There are alternative exporters available commercially, like the [Better FBX Add-on](https://blendermarket.com/products/better-fbx-importer--exporter){target=_blank}.  

>!!!warning
    In case your character geometry has a subdivision surface modifier in the stack or similar generative modifiers, you need to uncheck the option **Apply Modifiers**!  
    In case you want to export the subdivision levels, you will need to apply the modifier before exporting. This will work with shape keys too.  
    ![exp00](img/export/export_fbx_00.jpg)  
    *Apply Modifiers will prevent shape keys from exporting (Enabled by Default).*  

>![exp00](img/export/export_fbx_02.jpg)  
>*FBX export settings for shape keys.*  




