# Geometry
Your character geometry may consist of all kinds of different parts. The facial parts that are supported in the **automatic binding and animation** process include:  

>1. **Main Head/Face** (skin)
>2. **Eyes**
>3. **Teeth & Gum**
>4. **Tongue**
>5. **Other Geometry** like Facial Hair (eyelashes, brows, beards) or eye-shells, tearline etc.

!!!tip "One or Multiple Objects"
    The facial geometry might be split in separate objects or grouped in one object. Faceit can handle both.

## Requirements to Geometry

Although Faceit is supposed to work with any arbitrary character geometry, certain prerequisites have to be defined, regarding the geometry. These rules do not affect the general shape and morphology of the character. Faceit works on realistic, cartoon or even anthropomorphic models.  

>1. **Position & Orientation** The character has to be placed in the center of the scene (X,Y) and face the -Y direction. The height (Z) doesn't matter.  

___

>2. **Face Geometry** The character has to have a main face mesh (skin). This mesh should consist of a connected surface. In other words, all vertices in the face should be connected by edges. (*Other facial parts, like teeth, tongue, eyeballs, hair, etc. do not need to be connected and can also be part of the same object.*)  

___

### Best Practices

>1. **Neutral Expression** The character should be modeled in a neutral expression (Eyes open, mouth shut).  

___

>2. **Mirror Modifiers** can lead to problems in the rigging process. You should consider to apply them. Note, applying a modifier fails when there are shape keys on the mesh. Check out the [Apply Modifiers (with shape keys)](utils.md#apply-modifier-with-shape-keys) operator to keep your shape keys safe.

___

>3. **Surface Deform Modifiers** should be applied or disabled. (see 2)  

___

>4. **Animations** The character should not be animated when starting the rigging process. It is no problem if the objects are already attached to a body rig. If so, simply put the armature to Rest Position in Armature settings. This will suppress the animation. Alternatively, add a fake user to the action and remove it until you are done with rigging.  

___

>5. **Clean Topology** As Faceit involves regular rigging and animation workflows it is important to present clean topology with nice edge flow. You should present clean geometry, without double vertices and similar artefacts. You can find a lot of recources on *good* facial topology. [Here is one of them.](http://wiki.polycount.com/wiki/FaceTopology){target=_blank}  

It is important to note that the add-on is targeting organic characters and deformation, thus support for hard-surface objects (e.g. robots) is not intended. Note that rigid parts can be defined in [Setup](setup.md#register-geometry), but those won't move at all. However, you **can** bind rigid parts manually to the rig after the Faceit Bind Operator has been executed.  

!!! tip
    If your character model already has an ARKit compatible expression set, these requirements do not necessarily apply. In fact, you can skip the entire rigging process and instead head to the motion capture workflow directly. [Register the objects](setup.md#1-register-geometry) in setup tab and the [shape keys in the target shape list](target_shapes.md). Afterwards, you can start animating with motion capture.
