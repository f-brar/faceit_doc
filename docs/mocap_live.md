# Motion Capture - Live Recorder

The **Live Recorder** allows to receive animation data from [Face Cap](mocap_general.md#face-cap), [Hallway Tile](mocap_general.md#hallway-tile), [Live Link Face](mocap_general.md#live-link-face) or [iFacialMocap](mocap_general.md#ifacialmocap) and animate the active character in real-time. Head rotation and location as well as shape key data can be animated and recorded in real-time. After stopping the receiver, the animation data can be imported directly into a Blender action, on the active character or the [control rig](control_rig.md). 

![osc_panel](img/mocap/live-00.png)  


## Setup  

1. Make sure that your character is properly registered for mocap [(see Mocap Setup)](mocap_setup.md).  
1. Both the computer running Faceit and the device running the capturing application should be on the same network or on the same device.  
2. Blender should be allowed to communicate in the firewall settings of your computer.  
3. Depending on your sending application settings, you might need to change the default [port and address settings](#application-settings).
2. Leave the address field at the default `0.0.0.0` to make sure that all data on your network is received. To receive data from a specific device only, enter the IP-address of that device in the **Address** field.  
3. Enter a specific port or leave it at the default for each app (It's important to send and receive on the same port).
4. See the [Network Troubleshooting section](#network-troubleshooting-guide) below for common issues and how to solve them.  

![osc_address](img/mocap/osc/address.png)  
*Address and port settings in Faceit.*  

## Application Settings

### Face Cap

!!!tip ""
    Read more about Face Cap App [here](mocap_general.md#face-cap-app).  

1. Tap the `Go Live`button in the Face Cap main menu.  
2. Tap the `Connect/Disconnect` button in the following menu.  
3. In the `Server IP & Port` settings that pop up, do the following..  
4. In the first field, enter the (local) [IP-address](#how-do-i-find-my-ip-address) of your PC/mac running Blender.  
5. In the second field, enter the same number that you entered in the Faceit port field (default `9001`).  
6. Hit `Connect` to start streaming data to the PC.  
7. [Start the receiver in Faceit.](#start-capturing)  

???+ warning "In-App Purchases"  
    The Face Cap app itself is free-to-use, but the live streaming functionality is time capped to 5 seconds. You can unlock unlimited streaming via an in-app purchase (34.99$ - price may vary based on your region).  


>![face_cap_osc](https://www.bannaflak.com/face-cap/images/panel_livemode.png)  
*Connect/Disconnect: Opens a popup to start or stop a live mode sesstion.*  

>![SetupOSC](img/mocap/facecap/face_cap_live_03.jpg)  
*Enter IP address of your PC and the same port as in Faceit.*

___

### Live Link Face  

1. Go to Settings.
2. Make sure the capture mode is set to `Live Link (ARKit)`.  
3. Under `Streaming/Live Link` add a new `target`.  
4. Enter your PCs `IP address`.  
5. Leave the port at `11111` if you don't have a specfic reason for changing it.  
6. The live link face app is sending to the targets by default. There is no need to start the process.  
7. [Start the receiver in Faceit.](#start-capturing)  

![llf](img/mocap/live-link-face.jpeg)  
*A quick setup is required before the data can be received.*    
___

### Hallway Tile

1. Go to the `OSC` settings.  
2. You can leave the `Host` field at the default `0.0.0.0`. Alternatively, you may enter the IP address of your computer running Blender.  
3. The `Port` number should match the port number entered in Blender (default `9001`).  
4. Check `Enable OSC Streaming`.  
5. It doesn't matter witch character you choose in Hallway Tile. The sent data will be the same.  
6. [Start the receiver in Faceit.](#start-capturing)  

>![hallway_osc](img/mocap/osc/hallway_osc.png)  
*If you are running Faceit and Tile on the same device, you can keep the default settings.*  

___

### iFacialMocap

1. Open `Settings`.  
2. Enter your PCs `IP address` as target in the respective field.  
3. Default `port` is 49983. Make sure that the port in Faceit and iFacialMocap match.  
4. [Start the receiver in Faceit.](#start-capturing)  

!!!tip ""  
    You don't need to install the iFacialMocap software or the Blender add-on. Faceit can receive the data directly.  


## Start Capturing

1. Make sure that the [capturing application is sending data](#application-settings).  
3. Hit `Start Receiver` in Faceit.  
4. The character should now move in real-time.  
5. Hit `Stop Receiver` to stop the recording.  
6. If animation data was received, you can now import it onto your character.  

!!!warning
    The data won't be imported automatically. You need to execute the import operator after stopping the receiver.  

![options](img/mocap/live-settings.gif)  
*Check the options dropdown for filtering and smoothing options.*  

## Improve Live Performance

A laggy real-time playback does not necessarily mean that the recording is choppy. The received data is *recorded* on a separate thread and after importing the recorded frames to an action it should contain smooth continuos motion. It might still be desirable in many scenarious to have a smooth real-time playback. There are multiple factors that contribute to the quality and speed of real-time animation in the viewport! Read on below.  

1. **Custom Normals** and **Auto Smooth**:  
     * These functions are very demanding and can slow playback performance immensely!
     * [This neat little script by Artell](http://www.lucky3d.fr/auto-rig-pro/doc/auto_rig.html#clearing-custom-normals-and-auto-smoothing){target=_blank} will quickly remove them from your scene:  
```python
import bpy
​
def set_active_object(object_name):
    bpy.context.view_layer.objects.active = bpy.data.objects[object_name]
    bpy.data.objects[object_name].select_set(state=1)

for obj in bpy.data.objects:
    if obj.type != "MESH":
        continue
​
    try:
        set_active_object(obj.name)
    except:
        continue
​
    obj.data.use_auto_smooth = False
​
    try:
        bpy.ops.mesh.customdata_mask_clear()
    except:
        pass
​
    bpy.ops.mesh.customdata_custom_splitnormals_clear()
```

1. **Hide Meshes**:  
    - Hide Objects with the Screen symbol, instead of the eye to exclude them from scene evaluation. This improves playback performance, because it reduces the amount of data that needs to be evaluated each frame.  
    ![hide](img/mocap/osc/hide_meshes.gif)  
2. **Clean Up Weights**:  
    - Use the Clean Vertex Groups operator to cleanup very small bone weights. This can efficiently improve scene performance.  
    ![cleanup_weights](img/mocap/osc/cleanup_weights.gif)  
3. **Playback Options**:  
    - Set the sync mode to `Frame Dropping` to let Blender automatically drop frames for better playback.  
    - Only **play in** the Active Editor, to prevent evaluation of animation data in all animation editors.  
    >![playback](img/mocap/osc/sync_mode.png)  
4. **Simplify Scene**
    - Maybe you have a subdivision surface modifier in the stack or a multires modifier. I like to use the simplify settings to quickly toggle all modifiers on/off. [Click here to learn more about simplify settings](https://docs.blender.org/manual/en/latest/render/cycles/render_settings/simplify.html){target=_blank}  
    ![simplify](img/mocap/osc/simplify.png)  
5. **Rendermode**  
    - Eevee shaders, and even more so Cycles viewport render, can slow down playback immensely! Check if it works faster in solid render mode.  
    ![solid](img/mocap/osc/solid.png)  
6. The OSC data is streamed over your Wifi network. You should aim to optimize streaming performance. Usually this shouldn't be the bottleneck on modern setups.  

## Troubleshooting

>Receiving animation data from the local network is quite simple, but there are a few pitfalls to be aware of.

#### How do I find my IP address? 

- [Find local IP on Windows 7/10/11](https://www.whatismybrowser.com/detect/what-is-my-local-ip-address){target=_blank}  
- [Find local IP on MacOS](https://ccm.net/computing/macos/1409-how-to-the-find-public-or-local-ip-address-on-mac/){target=_blank}  
- [Find local IP on iPhone](https://www.macinstruct.com/tutorials/how-to-find-your-iphones-ip-address/){target=_blank}  


#### It sais "No recorded Data found."  

This is most likely a network issue. Make sure that you followed the [Network Setup](#network-setup) correctly. Read on below for some additional tips.  
>1. Make sure that your PCs Firewall is not blocking connections for Blender.  
>>- Allow/Block programs in Windows10  
>>- Allow/Block programs in MacOS  
>2. Make sure that both the streaming device and the PC running Blender are in the same local network. If you are unsure if the devices are in the same network you can [quickly execute a ping test and check if it is successful](https://www.siteground.com/kb/how_to_perform_ping_checks_in_windows_linux_and_mac_os/){target=_blank}.  
>3. Read all about address & port configuration [here](#osc-setup-port-address).


#### Help! The network performance is bad / laggy / choppy!  

- If the performance is bad and you are sure that it is related to the network, it might help to restart the router and give it some time to cool off.  
- Bad performance might also be caused by the scene complexity in Blender. Read more about optimizing performance [here](#improve-live-performance).  
- Lastly, if you are using Face Cap, your iPhone might be the bottleneck. The phone running hot can impact the stream performance. Giving it some time to cool down between shots can help.  

