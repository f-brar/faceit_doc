# Utilities

[TOC]  

## Rig Utils
This panel bundles rigging utilities beyond the regular Faceit rigging process for expression creation.  

![bake](img/other/rig_utils_join.jpg)  
*Quickly merge the Faceit rig with a complex armature.*  

### Join to Body (Destructive)
This operator allows to join the Faceit (Rigify) armature to any body armature. The most important thing to note is, that the operator will automatically merge the weights of both armatures. The resulting rig can directly be used for regular keyframe animation.

!!!warning "Destructive"
    The merge operation is destructive. It won't be possible to use the 'back to landmarks' operator or re-bind the Faceit weights. You should only proceed if you are happy with the bone placement and weights. It's still possible to generate and bake expressions.


![bake](img/other/merge_rigs_01.gif)  
*Quickly merge the Faceit rig with a complex armature.*  

!!!warning "Export Problems"
    Joining the complex Rigify bone structure from the Faceit rig into other armatures can cause problems during export or later down the pipeline when used in game engines. Make sure to save a copy and run some tests.  

![bake](img/other/unknown.png)  
*Full support for Auto Rig Pro armatures. Faceit bones will receive an ARP custom tag if joined to an ARP armature*  

## Shape Key Utils

The Shape Key Slider ranges are set automatically by default. It's recommended to leave this option untouched, but you can also manually set the shape key ranges on the registered or selected objects.  

## Other

### Apply Modifier (with Shape Keys)
Sometimes it's important to remove a modifier from certain objects. For example: Mirror modifiers have to be removed before starting the Faceit process, because they prevent the shape keys from working as expected after baking. This operator allows you to remove modifiers even though there are shape keys on the objects. Shape keys will be preserved!  
